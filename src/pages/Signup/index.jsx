import { Content, Background, Container, AnimationContainer } from "./styles";
import Button from "../../components/Button";
import { Link, Redirect, useHistory } from "react-router-dom";
import Input from "../../components/Input";
import {
  FiUser,
  FiMail,
  FiLock,
  FiSmile,
  FiPhone,
  FiCpu,
} from "react-icons/fi";
import { useForm } from "react-hook-form";
import * as yup from "yup";
import { yupResolver } from "@hookform/resolvers/yup";
import api from "../../services/api";
import { toast } from "react-toastify";

export default function Signup({ authenticated }) {
  const history = useHistory();
  const schema = yup.object().shape({
    email: yup.string().email("Email inválido").required("Campo obrigatório"),
    name: yup.string().required("Campo obrigatório"),
    bio: yup.string().required("Campo obrigatório"),
    course_module: yup.string().required("Campo obrigatório"),
    contact: yup.string().required("Campo obrigatório"),
    password: yup
      .string()
      .min(8, "Mínimo de 8 dígitos")
      .matches(
        /^((?=.*[!@#$%^&*()\-_=+{};:,<.>]){1})(?=.*\d)((?=.*[a-z]){1})((?=.*[A-Z]){1}).*$/,
        "Senha deve conter ao menos uma letra maiúscula, uma minúscula, um número e um caracter especial!"
      )
      .required("Campo obrigatório"),
  });

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({
    resolver: yupResolver(schema),
  });

  const onSubmitionFunction = (data) => {
    api
      .post("/users", data)
      .then((_) => {
        toast.success("usuário criado com sucesso");
        return history.push("/login");
      })
      .catch((err) => toast.error("Verifique os dados e tente novamente"));
  };
  if (authenticated) {
    return <Redirect to="/dashboard" />;
  }

  return (
    <Container>
      <Background />
      <Content>
        <AnimationContainer>
          <form onSubmit={handleSubmit(onSubmitionFunction)}>
            <h1>Cadastro</h1>
            <Input
              register={register}
              icon={FiUser}
              label="Nome"
              placeholder="Seu nome"
              name="name"
              error={errors.name?.message}
            />
            <Input
              name="email"
              register={register}
              icon={FiMail}
              label="Email"
              placeholder="Seu melhor email"
              error={errors.email?.message}
            />
            <Input
              name="password"
              register={register}
              icon={FiLock}
              label="Senha"
              placeholder="Uma senha bem forte"
              type="password"
              error={errors.password?.message}
            />
            <Input
              name="bio"
              register={register}
              icon={FiSmile}
              label="Bio"
              placeholder="Fale mais sobre você"
              error={errors.bio?.message}
            />
            <Input
              name="contact"
              register={register}
              icon={FiPhone}
              label="Contato"
              placeholder="Uma maneira de entrar em contato"
              error={errors.contact?.message}
            />
            <Input
              name="course_module"
              register={register}
              icon={FiCpu}
              label="Módulo do curso"
              placeholder="Qual módulo você está?"
              error={errors.course_module?.message}
            />
            <Button type="submit">Enviar</Button>
            <p>
              Já tem uma conta? Faça seu <Link to="/login">login</Link>
            </p>
          </form>
        </AnimationContainer>
      </Content>
    </Container>
  );
}
