import React, { useState, useEffect } from "react";
import { useForm } from "react-hook-form";
import { Redirect } from "react-router-dom";
import Button from "../../components/Button";
import Card from "../../components/Card";
import Input from "../../components/Input";
import {
  InputContainer,
  Container,
  TechsContainer,
  Header,
  AnimationContainer,
} from "./styles";
import api from "../../services/api";
import { toast } from "react-toastify";
import * as yup from "yup";
import { yupResolver } from "@hookform/resolvers/yup";

export default function Dashboard({ handleLogout, authenticated }) {
  const [techs, setTechs] = useState([]);
  const [token] = useState(
    JSON.parse(localStorage.getItem("@kenzieHub:token")) || ""
  );
  const [user] = useState(
    JSON.parse(localStorage.getItem("@kenzieHub:user")) || ""
  );

  function loadTechs() {
    api
      .get(`/users/${user.id}`, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then((resp) => setTechs(resp.data.techs))
      .catch((err) =>
        toast.error(
          "Verificar dados, Tecnologia já existente ou Nível inválido! Nível: Iniciante ou Intermediário ou Avançado"
        )
      );
  }

  const schema = yup.object().shape({
    title: yup.string().required("Campo obrigatório"),
    status: yup.string().required("Campo obrigatório"),
  });

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({
    resolver: yupResolver(schema),
  });

  useEffect(() => {
    loadTechs();
  });

  const onSubmit = (data) => {
    api
      .post("/users/techs", data, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then((resp) => {
        toast.success("Tecnologia adicionada com sucesso");
        loadTechs();
      })
      .catch((err) =>
        toast.error(
          "Verificar dados, Tecnologia já existente ou Nível inválido! Nível: Iniciante ou Intermediário ou Avançado"
        )
      );
  };

  function deleteTechs(key) {
    const newTechs = techs.filter((tech) => tech.id !== key);
    api
      .delete(`/users/techs/${key}`, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then((resp) => {
        setTechs(newTechs);
        toast.success("Tecnologia removida com sucesso");
      })
      .catch((_) => toast.error("Tente Novamente"));
  }

  if (!authenticated) {
    return <Redirect to="/login" />;
  }

  return (
    <Container>
      <Header>
        <h1>Bem Vindo: {user.name}</h1>
        <Button onClick={handleLogout}>Sair</Button>
      </Header>
      <InputContainer onSubmit={handleSubmit(onSubmit)}>
        <section>
          <h2>Cadastrar Nova Tecnologia</h2>
          <Input
            name="title"
            register={register}
            label="Tech:"
            placeholder="Nova Tecnologia"
            error={errors.title?.message}
          />
          <Input
            name="status"
            register={register}
            label="Nível:"
            placeholder="Qual seu nível?"
            error={errors.status?.message}
          />
          <Button type="submit">Cadastrar nova tecnologia</Button>
        </section>
      </InputContainer>
      <TechsContainer>
        {techs.map((tech) => (
          <AnimationContainer>
            <Card
              key={tech.id}
              id={tech.id}
              title={tech.title}
              status={tech.status}
              deleteTechs={deleteTechs}
            />
          </AnimationContainer>
        ))}
      </TechsContainer>
    </Container>
  );
}
