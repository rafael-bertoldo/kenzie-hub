import styled, { css } from "styled-components";

export const Container = styled.div`
  text-align: left;
  div {
    span {
      color: var(--red);
    }
  }
`;

export const InputContainer = styled.div`
  background: var(--white);
  border-radius: 10px;
  border: 2px solid var(--input-text);
  color: (--input-text);
  padding: 1rem;
  width: 100%;
  display: flex;
  transition: 0.4s;
  ${(props) =>
    props.isErrored &&
    css`
      border-color: var(--red);
    `}
  input {
    background: transparent;
    align-items: center;
    flex: 1;
    border: 0;
    color: var(--background);
    &::placeholder {
      color: var(--input-color);
    }
  }
`;
