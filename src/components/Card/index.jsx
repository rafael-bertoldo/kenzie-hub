import Button from "../Button";
import { Container } from "./styles";

function Card({ id, deleteTechs, title, status }) {
  // console.log(id);
  return (
    <Container>
      <span>Tech: {title}</span>
      <hr />
      <span>Nível: {status}</span>
      <Button onClick={() => deleteTechs(id)}>Deletar</Button>
    </Container>
  );
}

export default Card;
